# CMU 18731 HW2
# Code referenced from:git@bitbucket.org:huangty/cs144_bufferbloat.git
# Edited by: Deepti Sunder Prakash

#!/usr/bin/python

from mininet.topo import Topo
from mininet.node import CPULimitedHost
from mininet.link import TCLink
from mininet.net import Mininet
from mininet.log import lg, info
from mininet.util import dumpNodeConnections
from mininet.cli import CLI

from subprocess import Popen, PIPE
from time import sleep, time
from multiprocessing import Process
from argparse import ArgumentParser

import sys
import os

# Parse arguments

parser = ArgumentParser(description="Shrew tests")
parser.add_argument('--bw-host', '-B',
                    dest="bw_host",
                    type=float,
                    action="store",
                    help="Bandwidth of host links",
                    required=True)
parser.add_argument('--bw-net', '-b',
                    dest="bw_net",
                    type=float,
                    action="store",
                    help="Bandwidth of network link",
                    required=True)
parser.add_argument('--delay',
                    dest="delay",
                    type=float,
                    help="Delay in milliseconds of host links",
                    default='10ms')
parser.add_argument('-n',
                    dest="n",
                    type=int,
                    action="store",
                    help="Number of nodes in one side of the dumbbell.",
                    required=True)
# Expt parameters
args = parser.parse_args()

class DumbbellTopo(Topo):
    "Dumbbell topology for Shrew experiment"
    def build(self, n=6, bw_net=100, delay='20ms', bw_host=10):
	#TODO:Add your code to create the topology.
	s1 = self.addSwitch('s1')
	s2 = self.addSwitch('s2')
	hl1 = self.addHost('hl1')
	hl2 = self.addHost('hl2')
	hr1 = self.addHost('hr1')
	hr2 = self.addHost('hr2')
	a1 = self.addHost('a1')
	a2 = self.addHost('a2')
	self.addLink(s1, s2, bw=10, delay='20ms')
	self.addLink(hl1, s1, bw=10, delay='20ms')
	self.addLink(hl2, s1, bw=10, delay='20ms')
	self.addLink(a1, s1, bw=10, delay='20ms')
	self.addLink(hr1, s2, bw=10, delay='20ms')
	self.addLink(hr2, s2, bw=10, delay='20ms')
	self.addLink(a2, s2, bw=10, delay='20ms')
	
def bbnet():
    "Create network and run shrew  experiment"
    print "starting mininet ...."
    topo = DumbbellTopo(n=args.n, bw_net=args.bw_net,
                    delay='%sms' % (args.delay),
                    bw_host=args.bw_host)
    net = Mininet(topo=topo, host=CPULimitedHost, link=TCLink,
                  autoPinCpus=True)
    net.start()
    dumpNodeConnections(net.hosts)

    #TODO:Add your code to test reachability of hosts.
    net.pingAll()
    #TODO:Add your code to start long lived TCP flows between
    #hosts on the left and right.
    hl1, hl2, hr1, hr2 = net.get('hl1','hl2','hr1','hr2')
    hr1.cmd('iperf -s &')    
    hr2.cmd('iperf -s &')
    hl1.cmd('iperf -c '+hr1.IP()+'-t 60')
    hl2.cmd('iperf -c '+hr2.IP()+'-t 60')

    CLI(net)
    net.stop()

if __name__ == '__main__':
    bbnet()
